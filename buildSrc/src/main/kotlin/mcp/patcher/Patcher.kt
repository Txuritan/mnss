package mcp.patcher

import mcp.MCPExtension
import org.gradle.api.Project
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.charset.Charset
import java.util.Objects

class Patcher(private val project: Project) {
    fun run(vararg patches: Patch) {
        val mcpExt = project.extensions.getByType(MCPExtension::class.java)

        loop@ for (patch in patches) {
            when (patch.kind()) {
                PatchKind.FILE -> {
                    // TODO: check if file contents match
                    val contents = patch.file()

                    if (Objects.isNull(contents)) {
                        continue@loop
                    }

                    val file = patch.patch(mcpExt.mcpFolder)

                    project.logger.lifecycle("Patching ${file.absolutePath}")

                    file.writeBytes(contents!!.toByteArray(Charset.defaultCharset()))
                }
                PatchKind.LINE -> {
                    // TODO: check each line to see if its already been patched
                    val contents = patch.lines()

                    if (Objects.isNull(contents)) {
                        continue@loop
                    }

                    val file = patch.patch(mcpExt.mcpFolder)

                    project.logger.lifecycle("Patching ${file.absolutePath}")
                }
            }
        }
    }
}