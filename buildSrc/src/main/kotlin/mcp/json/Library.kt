package mcp.json

data class Library (
    val downloads: LibraryDownloads,
    val name: String,
    val rules: List<Rule>? = null,
    val extract: Extract? = null,
    val natives: Natives? = null
)