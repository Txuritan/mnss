package mcp.json

import com.beust.klaxon.Json

data class Classifiers (
    @Json(name = "natives-linux")
    val nativesLinux: Client? = null,

    @Json(name = "natives-osx")
    val nativesOsx: Client,

    @Json(name = "natives-windows")
    val nativesWindows: Client? = null
)