package mcp.internal

enum class Kind {
    CLASSES,
    SOURCES,
}