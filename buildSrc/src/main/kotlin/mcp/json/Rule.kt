package mcp.json

data class Rule (
    val action: String,
    val os: Os? = null
)