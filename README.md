# MNSS (Massive Networked Storage System)

MNSS is a Forge mod for Minecraft 1.2.5 based off Applied Energetics and Refined Storage.

Originally made for myself for use in Tekkit Classic.

## Building

Run `./gradlew setup` to setup the MCP/Forge workspace.

Run `./gradlew buildClient` or `./gradlew buildServer` depending on which side you want.
