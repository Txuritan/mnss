package mcp.tasks.internal

import mcp.MCPExtension
import mcp.internal.Utils
import mcp.internal.Side
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.file.SourceDirectorySet
import org.gradle.api.plugins.JavaPluginConvention
import org.gradle.api.tasks.SourceSet
import java.io.File
import java.io.IOException
import java.nio.file.Paths

open class BuildTask : DefaultTask() {
    private val copiedSources: MutableList<File> = mutableListOf()

    private val regex = Regex(".*(?:sources[\\\\/](?:client|common|server)[\\\\/]src[\\\\/]main[\\\\/](?:.*)[\\\\/]net[\\\\/]minecraft[\\\\/]src)[\\\\/](.*)")

    fun build(side: Side) {
        val mcpExt: MCPExtension = project.extensions.getByType(MCPExtension::class.java)

        // Get the source sets for the server and common code
        val sidedSources = sourcesFrom(side.getProject(project))
        val commonSources = sourcesFrom(mcpExt.commonProject!!)

        val sourceDir = Paths.get(mcpExt.mcpFolder.absolutePath, "src", side.minecraftPath(), "net", "minecraft", "src").toFile()

        project.logger.lifecycle("Copying mod source code")

        // Copy over all the common code
        copySources(commonSources.java, sourceDir)

        // Copy over all the sided code
        copySources(sidedSources.java, sourceDir)

        // Recompile MCP sources
        project.logger.lifecycle("Recompiling Minecraft")
        project.exec {
            it.workingDir(project.file(mcpExt.mcpFolder))

            it.commandLine(Utils.exec("recompile"))
        }

        // ReObfuscate MCP sources
        project.logger.lifecycle("Reobfuscating Minecraft")
        project.exec {
            it.workingDir(project.file(mcpExt.mcpFolder))

            it.commandLine(Utils.exec("reobfuscate"))
        }

        // Remove copied sources/resources
        project.logger.lifecycle("Removing copied mod source code")
        remove()

        // Copy mod resources to reobfuscation directory
        val reobfDir = Paths.get(mcpExt.mcpFolder.absolutePath, "reobf", side.minecraftPath()).toFile()
        project.logger.lifecycle("Copying mod resources to MCP reobf dir")
        copyResources(sidedSources.resources, reobfDir)
        copyResources(commonSources.resources, reobfDir)
    }

    private fun copySources(sources: SourceDirectorySet, folder: File) {
        sources.files.forEach { sourceFile ->
            val path = regex.find(sourceFile.absoluteFile.path)!!.groupValues.last()
            val newFile = Paths.get(folder.absolutePath, path).toFile()

            project.logger.lifecycle("Copying ${sourceFile.absolutePath} to ${newFile.absolutePath}")

            try {
                sourceFile.copyTo(newFile)

                copiedSources.add(newFile)
            } catch (ioe: IOException) {
                project.logger.error("Unable to copy source to MCP folder", ioe)
            }
        }
    }

    private fun copyResources(sources: SourceDirectorySet, folder: File) {
        sources.files.forEach { sourceFile ->
            val path = regex.find(sourceFile.absoluteFile.path)!!.groupValues.last()
            val newFile = Paths.get(folder.absolutePath, path).toFile()

            project.logger.lifecycle("Copying ${sourceFile.absolutePath} to ${newFile.absolutePath}")

            try {
                sourceFile.copyTo(newFile)
            } catch (ioe: IOException) {
                project.logger.error("Unable to copy resource to MCP folder", ioe)
            }
        }
    }

    private fun remove() {
        copiedSources.forEach { sourceFile ->
            project.logger.lifecycle("Removing ${sourceFile.absolutePath}")

            if (sourceFile.exists()) {
                if (!sourceFile.delete()) {
                    project.logger.error("Unable to delete source file `${sourceFile.path}`")
                }
            } else {
                project.logger.warn("Source file `${sourceFile.path}` does not exist")
            }
        }
    }

    private fun sourcesFrom(sourceProject: Project): SourceSet {
        val plugin = sourceProject.convention.findPlugin(JavaPluginConvention::class.java)
        val sourceSets = plugin!!.sourceSets

        return sourceSets.getByName(SourceSet.MAIN_SOURCE_SET_NAME)
    }
}