package mcp.internal

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.net.URL
import java.nio.channels.Channels
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.util.zip.ZipFile

object Utils {
    fun downloadFile(url: String, file: File) {
        file.outputStream().channel.transferFrom(Channels.newChannel(URL(url).openStream()), 0, Long.MAX_VALUE)
    }

    fun exec(command: String, useCmd: Boolean = false): List<String> {
        return when (OS.current()) {
            OS.WINDOWS -> if (useCmd) {
                listOf("cmd", "/c", ".\\${command}.cmd")
            } else {
                listOf("cmd", "/c", ".\\${command}.bat")
            }
            else -> listOf("sh", "./${command}.sh")
        }
    }

    fun unzipFernflower(zipFile: File, fernflowerJar: File): Boolean {
        val zip = ZipFile(zipFile)

        for (entry in zip.entries().toList()) {
            if (entry.name.startsWith("runtime") && entry.name.endsWith("fernflower.jar")) {
                val inputStream = zip.getInputStream(entry)

                Files.copy(inputStream, fernflowerJar.toPath(), StandardCopyOption.REPLACE_EXISTING)

                inputStream.close()

                zip.close()

                return true
            }
        }

        return false
    }

    fun unzipJar(jarFile: File, folder: File) {
        val zip = ZipFile(jarFile)

        zip.entries().toList().filter { (!it.isDirectory) && (!it.name.startsWith("META-INF")) }.forEach { entry ->
            Paths.get(folder.absolutePath, entry.name).toFile().let { file ->
                file.parentFile.mkdirs()

                val inputStream = BufferedInputStream(zip.getInputStream(entry))
                val outputStream = BufferedOutputStream(file.outputStream())

                val buffer = ByteArray(inputStream.available())

                inputStream.read(buffer)
                outputStream.write(buffer)

                inputStream.close()
                outputStream.close()
            }
        }
    }

    fun unzipToFolder(zipFile: File, folder: File) {
        val zip = ZipFile(zipFile)

        zip.entries().toList().filter { !it.isDirectory }.forEach { entry ->
            Paths.get(folder.absolutePath, entry.name).toFile().let { file ->
                file.parentFile.mkdirs()

                val inputStream = BufferedInputStream(zip.getInputStream(entry))
                val outputStream = BufferedOutputStream(file.outputStream())

                val buffer = ByteArray(inputStream.available())

                inputStream.read(buffer)
                outputStream.write(buffer)

                inputStream.close()
                outputStream.close()
            }
        }

        zip.close()
    }
}
