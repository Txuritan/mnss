package mcp.json

data class LibraryDownloads (
    val artifact: Client? = null,
    val classifiers: Classifiers? = null
)