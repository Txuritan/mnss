package mcp.json

data class Os (
    val name: String,
    val version: String
)