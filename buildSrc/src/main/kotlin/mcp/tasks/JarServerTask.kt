package mcp.tasks

import mcp.MCPExtension
import org.gradle.jvm.tasks.Jar
import java.nio.file.Paths

open class JarServerTask : Jar() {
    init {
        group = "mcp"
        description = "Assembles the server mod containing server and common classes and resources"

        mustRunAfter("buildServer")

        project.afterEvaluate {
            val mcpExt = project.extensions.getByType(MCPExtension::class.java)

            archiveFileName.set("${mcpExt.serverProject!!.group}-${mcpExt.serverProject!!.version}-server.jar")

            from(Paths.get(mcpExt.mcpFolder.absolutePath, "reobf", "minecraft_server"))
        }
    }
}