package mcp.tasks

import mcp.MCPExtension
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class SetupTask : DefaultTask() {
    init {
        group = "mcp"
        description = "Runs Forge's MCP install script"

        dependsOn("setupMCP", "setupMinecraft", "setupForge", "setupMaven")
    }

    @TaskAction
    fun run() {
        val mcpExt = project.extensions.getByType(MCPExtension::class.java)

        if (!mcpExt.cacheFolder.exists() && !mcpExt.cacheFolder.mkdirs()) {
            project.logger.error("Unable to create Cache folder")
        }

        if (!mcpExt.mcpFolder.exists() && !mcpExt.mcpFolder.mkdirs()) {
            project.logger.error("Unable to create MCP folder")
        }
    }
}