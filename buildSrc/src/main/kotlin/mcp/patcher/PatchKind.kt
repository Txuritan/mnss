package mcp.patcher

enum class PatchKind {
    FILE,
    LINE,
}
