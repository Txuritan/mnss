package mcp.tasks

import mcp.MCPExtension
import org.gradle.jvm.tasks.Jar
import java.nio.file.Paths

open class JarClientTask : Jar() {
    init {
        group = "mcp"
        description = "Assembles the client mod containing client and common classes and resources"

        mustRunAfter("buildClient")

        project.afterEvaluate {
            val mcpExt = project.extensions.getByType(MCPExtension::class.java)

            archiveFileName.set("${mcpExt.clientProject!!.group}-${mcpExt.clientProject!!.version}-client.jar")

            from(Paths.get(mcpExt.mcpFolder.absolutePath, "reobf", "minecraft"))
        }
    }
}