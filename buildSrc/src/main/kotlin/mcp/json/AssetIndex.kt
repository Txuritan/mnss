package mcp.json

data class AssetIndex (
    val id: String,
    val sha1: String,
    val size: Long,
    val totalSize: Long,
    val url: String
)