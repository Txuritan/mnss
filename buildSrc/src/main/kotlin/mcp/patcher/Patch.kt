package mcp.patcher

import java.io.File

interface Patch {
    fun kind() : PatchKind

    fun patch(mcpDir: File): File

    fun file(): String?

    fun lines(): Map<Int, String>?
}