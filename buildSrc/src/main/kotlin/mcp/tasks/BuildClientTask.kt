package mcp.tasks

import mcp.tasks.internal.BuildTask
import mcp.internal.Side
import org.gradle.api.tasks.TaskAction

open class BuildClientTask : BuildTask() {
    init {
        group = "mcp"
        description = "Assembles the client mod, bundling the common code"

        // finalizedBy("jarClient")
    }

    @TaskAction
    fun run() {
        build(Side.CLIENT)
    }
}