package mcp.json

import com.beust.klaxon.Klaxon

data class Minecraft (
    val assetIndex: AssetIndex,
    val assets: String,
    val downloads: MinecraftDownloads,
    val id: String,
    val libraries: List<Library>,
    val mainClass: String,
    val minecraftArguments: String,
    val minimumLauncherVersion: Long,
    val releaseTime: String,
    val time: String,
    val type: String
) {
    fun toJson() = Klaxon().toJsonString(this)

    companion object {
        fun fromJson(json: String) = Klaxon().parse<Minecraft>(json)
    }
}

