package mcp.json

data class Client (
    val sha1: String,
    val size: Long,
    val url: String,
    val path: String? = null
)