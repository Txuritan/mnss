package mcp.tasks

import com.google.common.hash.Hashing
import com.google.common.io.Files
import mcp.MCPExtension
import mcp.internal.OS
import mcp.internal.Utils
import mcp.patcher.Patcher
import mcp.patcher.patches.ForgeInstallSHPatch
import mcp.patcher.patches.server.ConsoleCommandHandlerPatch
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.nio.file.Paths

open class SetupMCPTask : DefaultTask() {
    data class Pair(val url: String, val file: String, val hashMD5: String, val hashSHA1: String) {
        fun cache(ext: MCPExtension): File {
            return Paths.get(ext.cacheFolder.absolutePath, file).toFile()
        }
    }

    class Data(mcpExt: MCPExtension) {
        val mcp62 = Pair("https://download844.mediafire.com/s7cewimayokg/c6liau295225253/mcp62.zip", "mcp62.zip", "7BBE85C86AC0C182B341682AAEAF5647", "F183ED1ADA5A7EBA2996E818C0C1DFA380AE132C")
        val mcp70a = Pair("https://download1648.mediafire.com/5cayzd21t92g/hxui27dv5q4k8v4/mcp70a.zip", "mcp70a.zip", "8AAAAC6D7876C327A84CC4CCAF7834B2", "B0D8CA75FD129D60FDD990BA74EB26BE328BE8F3")

        val forge = Pair("https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.2.5-3.4.9.171/forge-1.2.5-3.4.9.171-src.zip", "forge-src.zip", "4DEC0A1749C97234FCCE276445F074BE", "335BEA2084189C1D29886192CF61FD7DD78438B5")

        val fernflowerCacheFile: File = Paths.get(mcpExt.cacheFolder.absolutePath, "fernflower.jar").toFile()
        val fernflowerMcpFile: File = Paths.get(mcpExt.mcpFolder.absolutePath, "runtime", "bin", "fernflower.jar").toFile()
    }

    init {
        group = "mcp"
        description = "Downloads/extracts the MCP and Forge's MCP source code"
    }

    @TaskAction
    @Suppress("UnstableApiUsage", "DEPRECATION") // For Guava's hashing functions
    fun run() {
        val mcpExt: MCPExtension = project.extensions.getByType(MCPExtension::class.java)
        val data = Data(mcpExt)

        if (!mcpExt.cacheFolder.exists() && !mcpExt.cacheFolder.mkdirs()) {
            project.logger.error("Unable to create Cache folder")
        }
        if (!mcpExt.mcpFolder.exists() && !mcpExt.mcpFolder.mkdirs()) {
            project.logger.error("Unable to create MCP folder")
        }

        // Download
        ifNotExists("Downloading the MCP", data.mcp62.cache(mcpExt)) {
            Utils.downloadFile(data.mcp62.url, data.mcp62.cache(mcpExt))

            assert(Files.hash(data.mcp62.cache(mcpExt), Hashing.md5()).toString().toUpperCase() == data.mcp62.hashMD5)
            assert(Files.hash(data.mcp62.cache(mcpExt), Hashing.sha1()).toString().toUpperCase() == data.mcp62.hashSHA1)
        }
        ifNotExists("Downloading Forge MCP source code", data.forge.cache(mcpExt)) {
            Utils.downloadFile(data.forge.url, data.forge.cache(mcpExt))

            assert(Files.hash(data.forge.cache(mcpExt), Hashing.md5()).toString().toUpperCase() == data.forge.hashMD5)
            assert(Files.hash(data.forge.cache(mcpExt), Hashing.sha1()).toString().toUpperCase() == data.forge.hashSHA1)
        }

        // Extract
        project.logger.lifecycle("Extracting the MCP to ${mcpExt.mcpFolder.absolutePath}")
        Utils.unzipToFolder(data.mcp62.cache(mcpExt), mcpExt.mcpFolder)

        val forgeFolder = Paths.get(mcpExt.mcpFolder.absolutePath, "forge").toFile()
        project.logger.lifecycle("Extracting Forge to ${forgeFolder.absolutePath}")
        Utils.unzipToFolder(data.forge.cache(mcpExt), mcpExt.mcpFolder)

        // Fernflower
        ifNotExists("Downloading the MCP that contains Fernflower", data.mcp70a.cache(mcpExt)) {
            Utils.downloadFile(data.mcp70a.url, data.mcp70a.cache(mcpExt))

            assert(Files.hash(data.mcp70a.cache(mcpExt), Hashing.md5()).toString().toUpperCase() == data.mcp70a.hashMD5)
            assert(Files.hash(data.mcp70a.cache(mcpExt), Hashing.sha1()).toString().toUpperCase() == data.mcp70a.hashSHA1)
        }
        ifNotExists("Extracting Fernflower to ${data.fernflowerCacheFile.absolutePath}", data.fernflowerCacheFile) {
            if (!Utils.unzipFernflower(data.mcp70a.cache(mcpExt), data.fernflowerCacheFile)) {
                project.logger.error("Failed to extract Fernflower from MCP 70a")
            }
        }
        ifNotExists("Coping Fernflower to ${data.fernflowerMcpFile.absolutePath}", data.fernflowerMcpFile) {
            data.fernflowerCacheFile.copyTo(data.fernflowerMcpFile)
        }

        project.logger.lifecycle("Patching Forge MCP source code")

        val patcher = Patcher(project)

        patcher.run(ConsoleCommandHandlerPatch())

        if (OS.current() == OS.WINDOWS) {
            patcher.run(ForgeInstallSHPatch())
        }
    }

    private fun ifNotExists(message: String, file: File, run: () -> Unit) {
        if (!file.exists()) {
            project.logger.lifecycle(message)

            run()
        }
    }
}