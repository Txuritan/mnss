package mcp.json

data class Natives (
    val linux: String,
    val osx: String,
    val windows: String
)