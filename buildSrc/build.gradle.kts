plugins {
    id("java-gradle-plugin")
    id("org.jetbrains.kotlin.jvm").version("1.3.70")
}

group = "mnss"
version = "1.0.0.0"

repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("com.beust:klaxon:5.4")
    implementation("com.google.guava:guava:23.0")

    gradleApi()
}

gradlePlugin {
    plugins {
        create("mcpPlugin") {
            id = "mcp.build-plugin"
            implementationClass = "mcp.MCPPlugin"
        }
    }
}

tasks {
    compileKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    compileTestKotlin {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}