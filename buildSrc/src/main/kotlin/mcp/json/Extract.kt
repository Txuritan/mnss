package mcp.json

data class Extract (
    val exclude: List<String>
)