# TODO

  - ~~Patch or circumvent Forge scripts to remove the unneeded `pause` command~~
  - ~~Fix server recompile error due to an incomplete patch~~
  - Fix forge client entity renderer patch
  - Fix forge client gui ingame patch
  - Fix forge client item renderer patch
  - Maybe flatten the Minecraft client and server dependencies into one for the common project
    - Maybe even read both directories and only use the files that are shared between them
