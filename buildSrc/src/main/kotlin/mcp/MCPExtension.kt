package mcp

import org.gradle.api.Project
import java.io.File
import java.nio.file.Paths

open class MCPExtension {
    var clientProject: Project? = null
    var commonProject: Project? = null
    var serverProject: Project? = null

    /** The folder that will contains MCP related resources */
    var pluginFolder: File = Paths.get(".mcp-plugin").toFile()

    /** The folder that downloaded files will be saved to */
    var cacheFolder: File = Paths.get(pluginFolder.absolutePath, "cache").toFile()
    /** The folder that will house the Maven repo containing Minecraft's jars */
    var mavenFolder: File = Paths.get(pluginFolder.absolutePath, "maven").toFile()
    /** The folder that the MCP will be extracted to */
    var mcpFolder: File = Paths.get(pluginFolder.absolutePath, "mcp").toFile()
}