package mcp.tasks.internal

import mcp.MCPExtension
import mcp.internal.Kind
import mcp.internal.Side
import org.gradle.jvm.tasks.Jar
import java.nio.file.Paths

open class MavenJarTask(side: Side, kind: Kind) : Jar() {
    init {
        val mcpExt = project.extensions.getByType(MCPExtension::class.java)

        when (side) {
            Side.CLIENT -> {
                when (kind) {
                    Kind.CLASSES -> {
                        archiveFileName.set("minecraft-client-1.2.5.jar")

                        from(Paths.get(mcpExt.mcpFolder.absolutePath, "bin", "minecraft"))
                    }
                    Kind.SOURCES -> {
                        archiveFileName.set("minecraft-client-1.2.5-sources.jar")

                        from(Paths.get(mcpExt.mcpFolder.absolutePath, "src", "minecraft"))
                    }
                }
            }
            Side.SERVER -> {
                when (kind) {
                    Kind.CLASSES -> {
                        archiveFileName.set("minecraft-server-1.2.5.jar")

                        from(Paths.get(mcpExt.mcpFolder.absolutePath, "bin", "minecraft_server"))
                    }
                    Kind.SOURCES -> {
                        archiveFileName.set("minecraft-server-1.2.5-sources.jar")

                        from(Paths.get(mcpExt.mcpFolder.absolutePath, "src", "minecraft_server"))
                    }
                }
            }
        }
    }
}