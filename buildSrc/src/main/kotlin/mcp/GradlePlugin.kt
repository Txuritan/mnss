package mcp

/**
 * A dump way to get Intellij to stop complaining about the unused plugin class
 */
annotation class GradlePlugin