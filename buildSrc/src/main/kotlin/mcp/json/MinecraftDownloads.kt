package mcp.json

import com.beust.klaxon.Json

data class MinecraftDownloads (
    val client: Client,
    val server: Client,

    @Json(name = "windows_server")
    val windowsServer: Client
)