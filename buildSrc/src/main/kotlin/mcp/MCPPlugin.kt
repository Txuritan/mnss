package mcp

import mcp.tasks.BuildClientTask
import mcp.tasks.BuildServerTask
import mcp.tasks.JarClientTask
import mcp.tasks.JarServerTask
import mcp.tasks.MCPRecomplieTask
import mcp.tasks.MCPReobfuscateTask
import mcp.tasks.SetupForgeTask
import mcp.tasks.SetupMCPTask
import mcp.tasks.SetupMavenClientClassesJarTask
import mcp.tasks.SetupMavenClientSourcesJarTask
import mcp.tasks.SetupMavenServerClassesJarTask
import mcp.tasks.SetupMavenServerSourcesJarTask
import mcp.tasks.SetupMavenTask
import mcp.tasks.SetupMinecraftTask
import mcp.tasks.SetupTask
import org.gradle.api.Plugin
import org.gradle.api.Project

@GradlePlugin
class MCPPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.extensions.create("mcp", MCPExtension::class.java)

        project.tasks.create("buildClient", BuildClientTask::class.java)
        project.tasks.create("buildServer", BuildServerTask::class.java)

        project.tasks.create("jarClient", JarClientTask::class.java)
        project.tasks.create("jarServer", JarServerTask::class.java)

        project.tasks.create("mcpRecompile", MCPRecomplieTask::class.java)
        project.tasks.create("mcpReobfuscate", MCPReobfuscateTask::class.java)

        project.tasks.create("setup", SetupTask::class.java)
        project.tasks.create("setupForge", SetupForgeTask::class.java)
        project.tasks.create("setupMaven", SetupMavenTask::class.java)
        project.tasks.create("setupMavenClientClassesJar", SetupMavenClientClassesJarTask::class.java)
        project.tasks.create("setupMavenClientSourcesJar", SetupMavenClientSourcesJarTask::class.java)
        project.tasks.create("setupMavenServerClassesJar", SetupMavenServerClassesJarTask::class.java)
        project.tasks.create("setupMavenServerSourcesJar", SetupMavenServerSourcesJarTask::class.java)
        project.tasks.create("setupMinecraft", SetupMinecraftTask::class.java)
        project.tasks.create("setupMCP", SetupMCPTask::class.java)

        project.afterEvaluate {
            val mcpExt = project.extensions.getByType(MCPExtension::class.java)

            if (mcpExt.mavenFolder.exists()) {
                val client = project.dependencies.create("com.mojang:minecraft-client:1.2.5")
                val server = project.dependencies.create("com.mojang:minecraft-server:1.2.5")

                val clientProject = mcpExt.clientProject
                val commonProject = mcpExt.commonProject
                val serverProject = mcpExt.serverProject

                if (clientProject != null) {
                    clientProject.dependencies.add("implementation", client)
                    clientProject.repositories.maven {
                        it.url = mcpExt.mavenFolder.toURI()
                    }
                }
                if (serverProject != null) {
                    serverProject.dependencies.add("implementation", server)
                    serverProject.repositories.maven {
                        it.url = mcpExt.mavenFolder.toURI()
                    }
                }
                if (commonProject != null) {
                    commonProject.dependencies.apply {
                        add("implementation", client)
                        add("implementation", server)
                    }
                    commonProject.repositories.maven {
                        it.url = mcpExt.mavenFolder.toURI()
                    }
                }
            }
        }
    }
}