package mcp.tasks

import mcp.internal.Kind
import mcp.tasks.internal.MavenJarTask
import mcp.internal.Side

open class SetupMavenServerSourcesJarTask : MavenJarTask(Side.SERVER, Kind.SOURCES) {}