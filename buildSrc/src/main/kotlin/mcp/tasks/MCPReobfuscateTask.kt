package mcp.tasks

import org.gradle.api.DefaultTask

open class MCPReobfuscateTask : DefaultTask() {
    init {
        group = "mcp"
        description = "Reobfuscate Minecraft's client and server using the MCP"
        mustRunAfter("mcpRecompile")
    }
}