plugins {
    id("java-library")
    id("mcp.build-plugin")
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_6
}

mcp {
    clientProject = project(":client")
    commonProject = project(":common")
    serverProject = project(":server")
}

allprojects {
    apply(plugin = "java-library")

    group = "mnss"
    version = "1.0.0.0"
}
