package mcp.tasks

import mcp.internal.Kind
import mcp.tasks.internal.MavenJarTask
import mcp.internal.Side

open class SetupMavenClientSourcesJarTask : MavenJarTask(Side.CLIENT, Kind.SOURCES) {}