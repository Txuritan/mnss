package mcp.internal

enum class OS {
    LINUX,
    OSX,
    WINDOWS;

    companion object {
        fun current(): OS {
            val os = System.getProperty("os.name").toLowerCase()

            return when {
                os.contains("win") -> WINDOWS
                os.contains("mac") -> OSX
                else -> LINUX
            }
        }
    }

    override fun toString(): String {
        return when (this) {
            LINUX -> "linux"
            OSX -> "osx"
            WINDOWS -> "windows"
        }
    }
}