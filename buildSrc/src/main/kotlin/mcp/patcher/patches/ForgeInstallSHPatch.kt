package mcp.patcher.patches

import mcp.patcher.Patch
import mcp.patcher.PatchKind
import java.io.File
import java.nio.file.Paths

class ForgeInstallSHPatch : Patch {
    override fun kind(): PatchKind {
        return PatchKind.FILE
    }

    override fun patch(mcpDir: File): File {
        return Paths.get(mcpDir.absolutePath, "forge", "install.cmd").toFile()
    }

    override fun file(): String? {
        return """
            @echo off
            ..\runtime\bin\python\python_mcp install.py
        """.trimIndent()
    }

    override fun lines(): Map<Int, String>? {
        return null
    }
}