package mcp.tasks

import com.beust.klaxon.Klaxon
import com.google.common.hash.Hashing
import com.google.common.io.Files
import mcp.json.Library
import mcp.MCPExtension
import mcp.json.Minecraft
import mcp.internal.OS
import mcp.internal.Utils
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.nio.file.Paths
import java.util.Objects

open class SetupMinecraftTask : DefaultTask() {
    data class Pair(val artifact: String, val file: String, val hashMD5: String, val hashSHA1: String) {
        fun cache(ext: MCPExtension): File {
            return Paths.get(ext.cacheFolder.absolutePath, file).toFile()
        }
    }

    class Data(val mcpExt: MCPExtension) {
        val minecraftJSON = "https://launchermeta.mojang.com/v1/packages/10d52c2d052078115cbbaf6e927b8a1af1ac206b/1.2.5.json"

        val jinputArtifact = Pair("net.java.jinput:jinput:2.0.5", "jinput.jar", "CC07D371F79DC4ED2239E1101AE06313", "39C7796B469A600F72380316F6B1F11DB6C2C7C4")
        val jinputNativeArtifact = Pair("net.java.jinput:jinput-platform:2.0.5", "jinput-natives.jar", "B168B014BE0186D9E95BF3D263E3A129", "385EE093E01F587F30EE1C8A2EE7D408FD732E16")
        val lwjglArtifact = Pair("org.lwjgl.lwjgl:lwjgl:2.9.0", "lwjgl.jar", "CE74486A7687AD7EA91DCC1FCD6977B8", "5654D06E61A1BBA7AE1E7F5233E1106BE64C91CD")
        val lwjglNativeArtifact = Pair("org.lwjgl.lwjgl:lwjgl-platform:2.9.0", "lwjgl-natives.jar", "30E99B9386040F387FD94C26C1AC64D3", "3F11873DC8E84C854EC7C5A8FD2E869F8AAEF764")
        val lwjglUtilArtifact = Pair("org.lwjgl.lwjgl:lwjgl_util:2.9.0", "lwjgl_util.jar", "6A0EEAF3451ED9646B7D61A9DD8B86CC *lwjgl_util.jar", "A778846B64008FC7F48EAD2377F034E547991699")

        val clientCacheFile: File = Paths.get(mcpExt.cacheFolder.absolutePath, "minecraft.jar").toFile()
        val serverCacheFile: File = Paths.get(mcpExt.cacheFolder.absolutePath, "minecraft_server.jar").toFile()
    }

    init {
        group = "mcp"
        description = "Downloads adn extracts Minecraft's client, server and their natives"

        mustRunAfter("setupMCP")
    }

    @TaskAction
    fun run() {
        val mcpExt: MCPExtension = project.extensions.getByType(MCPExtension::class.java)
        val data = Data(mcpExt)

        if (!mcpExt.cacheFolder.exists() && !mcpExt.cacheFolder.mkdirs()) {
            project.logger.error("Unable to create Cache folder")
        }

        // Download and use Minecraft's launcher JSON
        val jsonFile = Paths.get(mcpExt.cacheFolder.absolutePath, "1.2.5.json").toFile()

        if (!jsonFile.exists()) {
            project.logger.lifecycle("Downloading the Minecraft JSON data")

            Utils.downloadFile(data.minecraftJSON, jsonFile)
        }

        val jsonData = Klaxon().parse<Minecraft>(jsonFile)

        if (Objects.isNull(jsonData)) {
            throw NullPointerException("Unable to parse returned Minecraft launcher JSON data")
        }

        // Download the client and server
        game(data, jsonData!!)

        // Download the normal libraries
        libraries(data, jsonData.libraries)

        // Download the libraries that contain native libraries
        natives(data, jsonData.libraries)
    }

    private fun game(data: Data, jsonData: Minecraft) {
        // Download
        ifNotExists("Downloading the Minecraft client", data.clientCacheFile) {
            Utils.downloadFile(jsonData.downloads.client.url, data.clientCacheFile)
        }
        ifNotExists("Downloading the Minecraft server", data.serverCacheFile) {
            Utils.downloadFile(jsonData.downloads.server.url, data.serverCacheFile)
        }

        // Copy
        val mcpClientFile = Paths.get(data.mcpExt.mcpFolder.absolutePath, "jars", "bin", "minecraft.jar").toFile()
        val mcpServerFile = Paths.get(data.mcpExt.mcpFolder.absolutePath, "jars", "minecraft_server.jar").toFile()

        if (!mcpClientFile.parentFile.exists() && !mcpClientFile.parentFile.mkdirs()) {
            project.logger.error("Unable to create MCP binary folder")
        }

        ifNotExists("Coping Minecraft's client to ${mcpClientFile.absolutePath}", mcpClientFile) {
            data.clientCacheFile.copyTo(mcpClientFile)
        }
        ifNotExists("Coping Minecraft's server to ${mcpServerFile.absolutePath}", mcpServerFile) {
            data.serverCacheFile.copyTo(mcpServerFile)
        }
    }

    @Suppress("UnstableApiUsage", "DEPRECATION") // For Guava's hashing functions
    private fun libraries(data: Data, libraries: List<Library>) {
        // Download
        ifNotExists("Downloading Minecraft's JInput library", data.jinputArtifact.cache(data.mcpExt)) {
            getUrl(data, libraries, data.jinputArtifact)

            assert(Files.hash(data.jinputArtifact.cache(data.mcpExt), Hashing.md5()).toString().toUpperCase() == data.jinputArtifact.hashMD5)
            assert(Files.hash(data.jinputArtifact.cache(data.mcpExt), Hashing.sha1()).toString().toUpperCase() == data.jinputArtifact.hashSHA1)
        }
        ifNotExists("Downloading Minecraft's LWJGL library", data.lwjglArtifact.cache(data.mcpExt)) {
            getUrl(data, libraries, data.lwjglArtifact)

            assert(Files.hash(data.lwjglArtifact.cache(data.mcpExt), Hashing.md5()).toString().toUpperCase() == data.lwjglArtifact.hashMD5)
            assert(Files.hash(data.lwjglArtifact.cache(data.mcpExt), Hashing.sha1()).toString().toUpperCase() == data.lwjglArtifact.hashSHA1)
        }
        ifNotExists("Downloading Minecraft's LWJGL util library", data.lwjglUtilArtifact.cache(data.mcpExt)) {
            getUrl(data, libraries, data.lwjglUtilArtifact)

            assert(Files.hash(data.lwjglUtilArtifact.cache(data.mcpExt), Hashing.md5()).toString().toUpperCase() == data.lwjglUtilArtifact.hashMD5)
            assert(Files.hash(data.lwjglUtilArtifact.cache(data.mcpExt), Hashing.sha1()).toString().toUpperCase() == data.lwjglUtilArtifact.hashSHA1)
        }

        // Copy
        val mcpBinaryFolder = Paths.get(data.mcpExt.mcpFolder.absolutePath, "jars", "bin").toFile()

        val jinputFile = Paths.get(mcpBinaryFolder.absolutePath, "jinput.jar").toFile()
        val lwjglFile = Paths.get(mcpBinaryFolder.absolutePath, "lwjgl.jar").toFile()
        val lwjglUtilFile = Paths.get(mcpBinaryFolder.absolutePath, "lwjgl_util.jar").toFile()

        ifNotExists("Coping Minecraft's JInput library to ${mcpBinaryFolder.absolutePath}", jinputFile) {
            data.jinputArtifact.cache(data.mcpExt).copyTo(jinputFile)
        }
        ifNotExists("Coping Minecraft's LWJGL library to ${mcpBinaryFolder.absolutePath}", lwjglFile) {
            data.lwjglArtifact.cache(data.mcpExt).copyTo(lwjglFile)
        }
        ifNotExists("Coping Minecraft's LWJGL Util library to ${mcpBinaryFolder.absolutePath}", lwjglUtilFile) {
            data.lwjglUtilArtifact.cache(data.mcpExt).copyTo(lwjglUtilFile)
        }
    }

    @Suppress("UnstableApiUsage", "DEPRECATION") // For Guava's hashing functions
    private fun natives(data: Data, libraries: List<Library>) {
        // Download
        ifNotExists("Downloading Minecraft's native JInput library", data.jinputNativeArtifact.cache(data.mcpExt)) {
            getNativeUrl(data, libraries, data.jinputNativeArtifact)

            assert(Files.hash(data.jinputNativeArtifact.cache(data.mcpExt), Hashing.md5()).toString().toUpperCase() == data.jinputNativeArtifact.hashMD5)
            assert(Files.hash(data.jinputNativeArtifact.cache(data.mcpExt), Hashing.sha1()).toString().toUpperCase() == data.jinputNativeArtifact.hashSHA1)
        }
        ifNotExists("Downloading Minecraft's native LWJGL library", data.lwjglNativeArtifact.cache(data.mcpExt)) {
            getNativeUrl(data, libraries, data.lwjglNativeArtifact)

            assert(Files.hash(data.lwjglNativeArtifact.cache(data.mcpExt), Hashing.md5()).toString().toUpperCase() == data.lwjglNativeArtifact.hashMD5)
            assert(Files.hash(data.lwjglNativeArtifact.cache(data.mcpExt), Hashing.sha1()).toString().toUpperCase() == data.lwjglNativeArtifact.hashSHA1)
        }

        // Extract
        val mcpNativeFolder = Paths.get(data.mcpExt.mcpFolder.absolutePath, "jars", "bin", "natives").toFile()

        project.logger.lifecycle("Extracting Minecraft's native LWJGL library to ${mcpNativeFolder.absolutePath}")
        Utils.unzipJar(data.jinputNativeArtifact.cache(data.mcpExt), mcpNativeFolder)
        project.logger.lifecycle("Extracting Minecraft's native JInput library to ${mcpNativeFolder.absolutePath}")
        Utils.unzipJar(data.lwjglNativeArtifact.cache(data.mcpExt), mcpNativeFolder)
    }

    private fun ifNotExists(message: String, file: File, run: () -> Unit) {
        if (!file.exists()) {
            project.logger.lifecycle(message)

            run()
        }
    }

    private fun getUrl(data:Data, libraries: List<Library>, pair: Pair) {
        val library = libraries.find {
            it.name == pair.artifact
        }

        Utils.downloadFile(library!!.downloads.artifact!!.url, pair.cache(data.mcpExt))
    }

    private fun getNativeUrl(data:Data, libraries: List<Library>, pair: Pair) {
        val library = libraries.find {
            it.name == pair.artifact
        }

        val downloads = when (OS.current() ) {
            OS.LINUX -> library!!.downloads.classifiers!!.nativesLinux!!
            OS.OSX -> library!!.downloads.classifiers!!.nativesOsx
            OS.WINDOWS -> library!!.downloads.classifiers!!.nativesWindows!!
        }

        Utils.downloadFile(downloads.url, pair.cache(data.mcpExt))
    }
}