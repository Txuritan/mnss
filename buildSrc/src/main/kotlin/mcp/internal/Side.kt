package mcp.internal

import mcp.MCPExtension
import org.gradle.api.Project

enum class Side {
    CLIENT,
    SERVER;

    fun getProject(project: Project): Project {
        val mcpExt: MCPExtension = project.extensions.getByType(MCPExtension::class.java)

        return when (this) {
            CLIENT -> mcpExt.clientProject!!
            SERVER -> mcpExt.serverProject!!
        }
    }

    fun minecraftPath(): String {
        return when (this) {
            CLIENT -> "minecraft"
            SERVER -> "minecraft_server"
        }
    }

    override fun toString(): String {
        return when (this) {
            CLIENT -> "client"
            SERVER -> "server"
        }
    }
}