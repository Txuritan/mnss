package mcp.tasks

import com.google.common.hash.Hashing
import com.google.common.io.Files
import mcp.MCPExtension
import mcp.internal.Side
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import java.io.File
import java.nio.charset.Charset
import java.nio.file.Paths

open class SetupMavenTask : DefaultTask() {
    init {
        group = "mcp"
        description = "Setup the local Maven repo containing Minecraft"

        mustRunAfter("setupForge")
    }

    @TaskAction
    fun run() {
        handle(Side.CLIENT)
        handle(Side.SERVER)
    }

    private fun handle(side: Side) {
        project.logger.lifecycle("Setting up Maven repo for Minecraft's ${side.toString().capitalize()}")

        val mcpExt = project.extensions.getByType(MCPExtension::class.java)

        val mavenClientDir = Paths.get(mcpExt.mavenFolder.absolutePath, "com", "mojang", "minecraft-${side}").toFile()

        if (!mavenClientDir.exists()) {
            if (!mavenClientDir.mkdirs()) {
                project.logger.error("Unable to create Maven repo folder structure")
            }
        }

        project.logger.lifecycle("Writing Maven metadata")
        writeTextWithHash(Paths.get(mavenClientDir.absolutePath, "maven-metadata.xml").toFile(), metadata(side))

        val maven125Dir = Paths.get(mavenClientDir.absolutePath, "1.2.5").toFile()

        if (!maven125Dir.exists()) {
            if (!maven125Dir.mkdirs()) {
                project.logger.error("Unable to create Maven repo version folder")
            }
        }

        project.logger.lifecycle("Writing Minecraft metadata")
        writeTextWithHash(Paths.get(maven125Dir.absolutePath, "minecraft-${side}-1.2.5.pom").toFile(), pom(side))

        kotlin.run {
            val name = "minecraft-${side}-1.2.5.jar"
            val from = Paths.get(project.buildDir.absolutePath, "libs", name).toFile()
            val to = Paths.get(maven125Dir.absolutePath, name).toFile()

            project.logger.lifecycle("Coping jar from ${from.absolutePath} to ${to.absolutePath}")
            copyWithHash(from, to)
        }

        kotlin.run {
            val name = "minecraft-${side}-1.2.5-sources.jar"
            val from = Paths.get(project.buildDir.absolutePath, "libs", name).toFile()
            val to = Paths.get(maven125Dir.absolutePath, name).toFile()

            project.logger.lifecycle("Coping jar from ${from.absolutePath} to ${to.absolutePath}")
            copyWithHash(from, to)
        }
    }

    companion object {
        private fun metadata(side: Side): String {
            return """
                <?xml version="1.0" encoding="UTF-8"?>
                <metadata>
                  <groupId>com.mojang</groupId>
                  <artifactId>minecraft-${side}</artifactId>
                  <versioning>
                    <release>1.2.5</release>
                    <versions>
                      <version>1.2.5</version>
                    </versions>
                    <lastUpdated>20120404023255</lastUpdated>
                  </versioning>
                </metadata>
            """.trimIndent()
        }

        private fun pom(side: Side): String {
            return """
                <?xml version="1.0" encoding="UTF-8"?>
                <project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                  <modelVersion>4.0.0</modelVersion>
                  <groupId>com.mojang</groupId>
                  <artifactId>minecraft-${side}</artifactId>
                  <version>1.2.5</version>
                </project>
            """.trimIndent()
        }

        @Suppress("UnstableApiUsage", "DEPRECATION") // For Guava's hashing functions
        private fun copyWithHash(from: File, to: File) {
            Files.copy(from, to)

            Files.write(Files.hash(to, Hashing.md5()).asBytes(), Paths.get(to.absolutePath + ".md5").toFile())
            Files.write(Files.hash(to, Hashing.sha1()).asBytes(), Paths.get(to.absolutePath + ".sha1").toFile())
        }

        @Suppress("UnstableApiUsage", "DEPRECATION") // For Guava's hashing functions
        private fun writeTextWithHash(file: File, content: String) {
            Files.write(content.toByteArray(Charset.defaultCharset()), file)

            Files.write(Files.hash(file, Hashing.md5()).asBytes(), Paths.get(file.absolutePath + ".md5").toFile())
            Files.write(Files.hash(file, Hashing.sha1()).asBytes(), Paths.get(file.absolutePath + ".sha1").toFile())
        }
    }
}