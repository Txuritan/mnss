package mcp.tasks

import mcp.MCPExtension
import mcp.internal.Utils
import org.gradle.api.tasks.Exec
import org.gradle.api.tasks.TaskAction
import java.nio.file.Paths

open class SetupForgeTask : Exec() {
    init {
        group = "mcp"
        description = "Runs Forge's MCP install script"

        mustRunAfter("setupMinecraft")

        val mcpExt = project.extensions.getByType(MCPExtension::class.java)

        workingDir(Paths.get(mcpExt.mcpFolder.absolutePath, "forge").toFile())

        commandLine(Utils.exec("install", true))
    }

    @TaskAction
    fun run() {}
}