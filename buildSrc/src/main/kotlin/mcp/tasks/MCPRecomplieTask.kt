package mcp.tasks

import org.gradle.api.DefaultTask

open class MCPRecomplieTask : DefaultTask() {
    init {
        group = "mcp"
        description = "Recompiles Minecraft's client and server using the MCP"
    }
}